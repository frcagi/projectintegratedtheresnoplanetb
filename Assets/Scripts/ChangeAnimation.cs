﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeAnimation : MonoBehaviour
{
    public GameObject cielo;
    public GameObject montanyas;
    public GameObject suelo;
    public GameObject primerplano;
    public GameObject planoIntermedio;
    Animator anim;
    private void Awake()
    {
        anim = GetComponent<Animator>();
        cielo = GameObject.FindWithTag("Fondo");
        montanyas = GameObject.FindWithTag("Montanyas");
        suelo = GameObject.FindWithTag("Suelo");
        primerplano = GameObject.FindWithTag("PrimerPlano");
        planoIntermedio = GameObject.FindWithTag("Intermedio");
       
    }
    // Start is called before the first frame update
    void Start()
    {
      

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetTrigger("Saltar");
            movimientoObjetos(1.3f);

        }
       
        if (Input.GetAxis("Horizontal") > 0)
        {           
          
            anim.SetTrigger("Correr");                 
         movimientoObjetos(1.3f);
        }       

        else {
            anim.SetTrigger("idle");
        }
       
    }
    void movimientoObjetos(float speed) {
        cielo.transform.Translate(new Vector3((-1 * speed * Time.deltaTime), 0, 0));
        montanyas.transform.Translate(new Vector3((-1 * (speed+0.4f)  * Time.deltaTime), 0, 0));
        suelo.transform.Translate(new Vector3((-1 * (speed + 0.4f) * Time.deltaTime), 0, 0));
        primerplano.transform.Translate(new Vector3(-1 *(speed+1.3f)* Time.deltaTime, 0, 0));
        planoIntermedio.transform.Translate(new Vector3((-1 *(speed+0.6f)* Time.deltaTime), 0, 0));

    }
  
}
     


